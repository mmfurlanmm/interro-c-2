#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct Game //cr�ation de la structure du jeu
{
    int combinaison[4]; //tableau qui contiendra la combinaison g�n�r�e par l'ordinateur
    int combinaisonJoueur[4]; // tableau qui contiendra les combinaisons entr�es par le joueur
    int nbEssais;
    int partiesGagnees;
    int partiesPerdues;

};



int tabInit (int tab[4]) // fonction permettant d'initialiser un tableau
{
    int i;
    for (i=0; i<4; i++)
    {

        do
        {
            printf("Entrez le chiffre %d de votre combinaison : ", i+1);
            scanf("%d", &tab[i]);
            if (tab[i]>9 || tab[i]<0)
            {
                printf("entrez un chiffre entre 0 et 9 svp!\n");
            }

        }
        while (tab[i]>9||tab[i]<0);


    }
    return tab[i];
}

// Structure permettant d'effectuer la comparaison, mais je n'arrive pas � faire fonctionner le programme avec celle-ci...
/*
struct Resultat
{
    int nbChiffresBienPlaces;
    int nbChiffresPresents;
};

struct Resultat tabTest (int tab1[4], int tab2[4])
{
    int i;
    int j;

    struct Resultat resultatDuTest;

    resultatDuTest.nbChiffresBienPlaces = 0;
    resultatDuTest.nbChiffresPresents = 0;
    for (i=0; i<4; i++)
    {
        if (tab1[i] == tab2[i])
        {
            resultatDuTest.nbChiffresBienPlaces ++;
        }

        for (j=0; j<4; j++)
        {
            if ((tab1[i] == tab2[j]) && (j!=i))
            {
                resultatDuTest.nbChiffresPresents ++;
            }
        }




    }
    return resultatDuTest;

}*/



int main()
{
    srand(time(NULL));

    int i;
    int j;
    struct Game mastermind;
    mastermind.nbEssais;
    mastermind.partiesGagnees = 0;
    mastermind.partiesPerdues = 0;
    int joueurGagne = 0;
    int joueurRejoue = 0; //variable qui passe � 1 si le joueur gagne, c'est pas terrible mais je n'arrive pas � faire marcher le programme sans �a
    int nbChiffresBienPlaces = 0; //compteur des chiffres bien plac�s
    int nbChiffresPresents = 0; // compteur des chiffres apparaissant � un autre endroit dans la combinaison


    printf("**************************MASTERMIND**************************\n\n");

    do // boucle DO WHILE pour rejouer
    {

        //On commence par initialiser la combinaison de l'ordi avec 4 chiffres compris entre 0 et 9
        for (i=0; i<4; i++)
        {
            int chiffreAleatoire = rand()%10;
            mastermind.combinaison[i]= chiffreAleatoire;
        }

        mastermind.nbEssais = 10; //Initialisation du nombre d'essais, 10 comme demand�


        while ((mastermind.nbEssais > 0) && (joueurGagne!=1)) //cette boucle while contient l'algo de comparaison entre la combinaison de l'ordi et celle du joueur
        {


            //On demande au joueur d'entrer sa combinaison. On utilise la fonction tabInit permettant de remplir un tableau
            tabInit(mastermind.combinaisonJoueur);

            //On teste la combinaison du joueur
            for (i=0; i<4; i++)
            {
                if (mastermind.combinaisonJoueur[i] == mastermind.combinaison[i])
                {
                    nbChiffresBienPlaces ++;
                }

                else

                    for (j=0; j<4; j++)
                    {
                        if ((mastermind.combinaisonJoueur[i] == mastermind.combinaison[j]) && (j!=i))
                        {
                            nbChiffresPresents ++;
                        }
                    }

            }


            printf("\n%d chiffre(s) place(s) correctement\n", nbChiffresBienPlaces);
            printf("%d chiffre(s) apparai(ssen)t a un autre endroit dans la combinaison\n\n", nbChiffresPresents);


            if (nbChiffresBienPlaces == 4)
            {
                joueurGagne = 1;
            }
            else if (nbChiffresBienPlaces < 4)
            {
                mastermind.nbEssais--;

            }

            if (mastermind.nbEssais > 1 && nbChiffresBienPlaces < 4)
            {
                printf("il vous reste %d essais\n\n", mastermind.nbEssais);
            }
            else if (mastermind.nbEssais == 1 && nbChiffresBienPlaces < 4)
            {
                printf("il vous reste %d essai\n\n", mastermind.nbEssais);
            }

            //On r�initialise � 0 les varialbles nbChiffresBienPlaces et nbChiffresPresents afin que les infos soient correctes lors de l'essai suivant
            nbChiffresBienPlaces = 0;
            nbChiffresPresents = 0;

        }
        if (mastermind.nbEssais==0)
        {
            printf("Vous avez perdu\n\n");
            mastermind.partiesPerdues++; // Incr�mentation du nombre de parties perdues
        }
        else
        {
            printf("Vous avez gagne!\n\n");
            mastermind.partiesGagnees++; // Incr�mentation du nombre de parties perdues
        }

        printf("Nombre de parties gagnees : %d\n", mastermind.partiesGagnees);
        printf("Nombre de parties perdues : %d\n\n", mastermind.partiesPerdues);


        for (i=0; i<4; i++) //boucle pour r�initialiser � 0 la combinaison du joueur, dans le cas d'une autre partie
        {

            mastermind.combinaisonJoueur[i]= 0;
        }

        joueurGagne = 0;



        do // Boucle DO WHILE afin d'emp�cher une mauvaise entr�e lorsqu'on demande au joueur s'il souhaite rejouer
        {
            printf("Voulez-vous rejouer ? OUI = 1 / NON = 2 : ");
            scanf("%d", &joueurRejoue);

        }
        while (joueurRejoue < 1 || joueurRejoue > 2);





    }
    while (joueurRejoue == 1);


    return 0;
}
